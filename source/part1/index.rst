.. _part1:


*************************************************************************************************
Partie 1 | Lexical Analysis
*************************************************************************************************

Nanananananana BATMAN! 
======================
proposed by Group 33, Bastien Nothomb and Guillaume Everarts de Velp

Question
""""""""

Let's the regular expression that will stay in your mind for the rest of the day be:
	.. math::
		(NA)^+(BATMAN !)
		
For this expression :

* Use Thomson construct to derive the NFA recognizing the language
* Use the power set-construction for deriving an equivalent DFA
* Use the partitioning method to derive an equivalent minimal DFACreate the minimal DFA
* Create the table driven implementation

Answers
"""""""
		
NFA diagram:
	.. image:: img/33-nfa-thomson.png
		:alt: NFA
		

DFA diagram:
	.. image:: img/33-dfa.png
		:height: 200
		:alt: DFA

Minimized DFA diagram:
    .. image:: img/33-dfa-minimized.png
        :height: 200
        :alt: DFA minimized

Table Driven Implementation:

    +---------------+-----------+-----------+-----------+
    | input\\state  | S1        | S2        | S3        |
    +===============+===========+===========+===========+
    | NA            | S2        | S2        |           |
    +---------------+-----------+-----------+-----------+
    | BATMAN!       |           | S3        |           |
    +---------------+-----------+-----------+-----------+
		

White space, comments and division
==========================================================
by Group 28, Pablo Gonzalez Alvarez and Noémie Verstraete

You have the following code:

.. code-block:: java

        //...
            boolean moreWhiteSpace = true;
            while (moreWhiteSpace) {
                while (isWhitespace(ch)) {
                    nextCh();
                }
                if (ch == '/') {
                    nextCh();
                    if (ch == '/') {
                        // CharReader maps all new lines to '\n'
                        while (ch != '\n' && ch != EOFCH) {
                            nextCh();
                        }
                    } else {
                        return new TokenInfo(DIV, line);
                    }
                } else {
                    moreWhiteSpace = false;
                }
            }
        // ...

        private boolean isWhitespace(char c) {
            switch (c) {
            case ' ':
            case '\t':
            case '\n': // CharReader maps all new lines to '\n'
            case '\f':
                return true;
            }
            return false;
        }

Answer the following as precisely as you can:


1. Explain
""""""""""""""

Explain what the above given code does.

2. State transition diagram
"""""""""""""""""""""""""""

Draw the state transition diagram for the above given code.

Hint: see sections `White Space` and `Comments` of chapter 2 in the reference book.

3. Regular expression
"""""""""""""""""""""

Write a regular expression that describes the languages of Java whitespaces.

4. NFA and Thompson's construction
""""""""""""""""""""""""""""""""""""

Use the Thompson's construction to derive a NFA recognizing the same language as the one described by the following regular expression::

(w*|c*d*)*

5. DFA and powerset construction
""""""""""""""""""""""""""""""""
For the NFA in the previous question, use powerset construction for deriving an equivalent DFA;

6. Minimal DFA and partitioning method
""""""""""""""""""""""""""""""""""""""
For the DFA in the previous question, use the partitioning method for deriving an equivalent minimal DFA;

Bonus
""""""""""""""

Where does the above given code come from?

Hint: have a look into the `j-- src` folder.


Question on Lexical Analysis
==========================================================
**Proposed by Group 12 (Cyril Dénos & Julian Roussieau)**

Even though most lexical analyzers (i.e. scanners) are generated, it is possible to write it manually. Let's consider the grammar of java.

#. Draw a state transition diagram that is able to distinguish identifiers from reserved words and to recognize the following keywords:

	* char
	* case
	* int
	* import
	* catch

#. Write a possible corresponding java code.
