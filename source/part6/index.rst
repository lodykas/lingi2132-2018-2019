.. _part6:

*************************************************************************************************
Partie 6 | DSL and Scala
*************************************************************************************************

Question proposed by Group 05, Desausoi Laurent & Dhillon Sundeep
=================================================================

1. Explain the main differences between an object and a class in Scala.
-----------------------------------------------------------------------
2. Explain the difference between var and val. Is there one that we should prefereably use ? What could be their equivalent in Java?
------------------------------------------------------------------------------------------------------------------------------------
3. Define what currying is.
---------------------------
3.1. Apply the currying technique of the following uncurried function.
----------------------------------------------------------------------
.. code-block:: scala

  def minus(x: Int, y: Int): Int = {
    x - y
  }
4. Convert the following Java code (in which we simply multiply each element of 2 matrixes) into Scala code.
------------------------------------------------------------------------------------------------------------
.. code-block:: java

  public static int[][] matrixMultiply(int[][] a, int[][] b) {
    int[][] c = new int[a.length][a[0].length];

    for (int i = 0; i < a.length; i++)
        for (int j = 0; j < a[i].length; j++)
            c[i][j] = a[i][j] * b[i][j];
    return c;
  }


""""""""""""""

Answers
=======

1. An object is an instance of a class which is guaranteed to be unique at a certain time T (it follows the Singleton pattern). Once an object is created,
a class is more a definition, a description. It defines a type in terms of methods and composition of other types. In Scala, we create objects instead of classes.

2. Var is mutable (we can reassign its value) unlike val which is unmutable (but we can still change the object's state, its internal variable).
In Scala, we prefer to use val instead of var because it increases the readability.
In Java, the equivalent would be the following: val is a final variable and var just a "normal" variable.

3. It's a special technique thanks to which we can transform a function with several arguments and translate it into a sequence of functions, each with a single argument.
You can try turn any function with multiple arguments intos its curried equivalent.

3.1.

.. code-block:: scala

  def minus(x: Int): (Int => Int) = {
    (y: Int) => {
      x - y
    }
  }

Or equivalently:

.. code-block:: scala

  def minus(x: Int)(y: Int): Int = {
    x - y
  }

4.

.. code-block:: scala

  def matrixMultiply(a: Array[Array[Int]], b: Array[Array[Int]]): Array[Array[Int]] = {
    val c: Array[Array[Int]] = Array.ofDim[Int](a.length, a(0).length)
    for(i <- 0 until a.length; j <- 0 until a(i).length) {
      c(i)(j) = a(i)(j) * b(i)(j)
    }
    c
  }

""""""""""""""
